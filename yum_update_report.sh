#!/bin/bash
#####
## Author: jparrill@redhat.com
## Dept: Consulting/Infrastructure
## Detail: Script to check updates and their severity 
## Example: yum_update_report.sh -s Moderate
#####

function validations ()
{
	SEVERITY=$1

	case "${SEVERITY}" in
		All|all)			COMMAND="yum updateinfo list"
									;;

		Critical|critical)		COMMAND="yum updateinfo list --security --sec-severity=Critical"
									;;

		Important|important)		COMMAND="yum updateinfo list --security --sec-severity=Important"
									;;

		Moderate|moderate)	  	COMMAND="yum updateinfo list --security --sec-severity=Moderate"
									;;

		Bugfix|bugfix)			COMMAND="yum updateinfo list --bugfix"
									;;

		Enhancement|enhancement)	COMMAND="yum updateinfo list | grep enhancement"
									;;
	esac
}

function check_updates ()
{
	$COMMAND
}

function export ()
{
	$COMMAND | awk '{print $3}' | grep `uname -m` > /tmp/yum_pending_updates.log
}

function usage ()
{
cat <<EOF
usage: $0 options
 
OPTIONS:
	-s   Severity: All, Critical, Important, Moderate, Bugfix, Enhancement
	-h   Show Help
EOF
}
 
## Main
while getopts "s:n:ha" OPTION
do
    case $OPTION in
        h)
            usage
            exit
            ;;
        s)
            SEVERITY=$OPTARG
            ;;
        ?)
            usage
            exit
            ;;
    esac
done
 
if [[ -z $SEVERITY ]] 
then
    usage
    exit 1
else
	validations $SEVERITY
    check_updates
fi